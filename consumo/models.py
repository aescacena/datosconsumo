from django.db import models
import uuid

# Create your models here.

class consumo(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text="ID único para este consumo particular")
    date = models.DateTimeField(null=True, blank=True)
    energy = models.FloatField(null=True, blank=True)
    reactiveEnergy = models.FloatField(null=True)
    power = models.FloatField(null=True)
    maximeter = models.FloatField(null=True)
    reactivePower = models.FloatField(null=True)
    voltage = models.FloatField(null=True)
    intensity = models.FloatField(null=True)
    powerFactor = models.FloatField(null=True)

    class Meta:
        ordering = ["date"]

    def __str__(self):
        return self.date.strftime('%Y-%m-%d %H:%M')