import datetime

from django.http import HttpResponseRedirect
from django.shortcuts import render
import csv, io
from django.contrib import messages

from . import forms
from .forms import formularioEjemplo
from .models import consumo as c
from datetime import date as django_date

# Create your views here.

def index(request):
    power = []
    date = []
    energy = []
    reactiveEnergy = []
    maximeter = []
    reactivePower = []
    voltage = []
    intensity = []
    powerFactor = []
    queryset = c.objects.all()

    for consumo in queryset:
        power.append(consumo.power)
        date.append(consumo.date.strftime("%Y-%m-%d"))
        energy.append(consumo.energy)
        reactiveEnergy.append(consumo.reactivePower)
        maximeter.append(consumo.maximeter)
        reactivePower.append(consumo.reactivePower)
        voltage.append(consumo.voltage)
        intensity.append(consumo.intensity)
        powerFactor.append(consumo.powerFactor)

    return render(request,
                  'index.html',
                  context={'power':power,
                           'date':date,
                           'energy':energy,
                           'reactiveEnergy':reactiveEnergy,
                           'maximeter':maximeter,
                           'reactivePower':reactivePower,
                           'voltage':voltage,
                           'intensity':intensity,
                           'powerFactor':powerFactor}
                )


def upload(request):

    data = c.objects.all()

    # GET request returns the value of the data with the specified key.
    if request.method == "GET":
        return render(request, "upload.html")
    csv_file = request.FILES['file']

    # let's check if it is a csv file
    if not csv_file.name.endswith('.csv'):
        messages.error(request, 'THIS IS NOT A CSV FILE')
    data_set = csv_file.read().decode('UTF-8')

    # setup a stream which is when we loop through each line we are able to handle a data in a stream
    io_string = io.StringIO(data_set)
    next(io_string)
    for column in csv.reader(io_string, delimiter=','):
        created = c.objects.update_or_create(
        date = datetime.datetime.strptime(column[0], "%d %b %Y %H:%M:%S"),
        energy = column[1],
        reactiveEnergy = column[2],
        power = column[3],
        maximeter = column[4],
        reactivePower = column[5],
        voltage = column[6],
        intensity = column[7],
        powerFactor = column[8]
    )
    context = {}
    return render(request, "index.html", context)