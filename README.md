# **APP DATOS DE CONSUMO**

## Quick start

```sh
$ # Get the code
$ git clone https://gitlab.com/aescacena/datosconsumo.git
$ cd datosconsumo
$ #
$ # Install modules
$ pip3 install -r requirements.txt
$ mkvirtualenv env
$ #
$ # Create tables
$ python manage.py makemigrations
$ python manage.py migrate
$ #
$ # Start the application (development mode)
$ python manage.py runserver # default port 8000
$ #
$ # Access the web app in browser: http://127.0.0.1:8000/
```

## User create
**User:** admin
**pass:** admin